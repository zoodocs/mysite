# -*- coding: utf-8 -*-
# @Time    : 2019/3/30 6:44
# @Author  : fuqiang
# @File    : urls.py
# @Desc    : app_base的路由

from django.contrib import admin
from django.urls import path
from app_base import views

app_name = 'app_base'
urlpatterns = [
    path('', views.login),
    path('login/', views.login),
    path('home/', views.home),
]

